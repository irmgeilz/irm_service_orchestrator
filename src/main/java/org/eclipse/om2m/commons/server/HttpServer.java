package org.eclipse.om2m.commons.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import obix.Obj;

import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletHandler;
import org.eclipse.om2m.commons.mapper.Mapper;
import org.eclipse.om2m.commons.resource.Notification;
import org.eclipse.om2m.commons.util.NotificationUtil;
import org.eclipse.om2m.commons.util.NotificationUtil.NotObixContentException;

public class HttpServer {

	private static Mapper mapper = new Mapper();

	private static int PORT = 1401;
	private static String CONTEXT = "/monitor";

	/**
	 * Get the payload as string
	 * 
	 * @param bufferedReader
	 * @return payload as string
	 */
	public static String getPayload(BufferedReader bufferedReader) {
		Scanner sc = new Scanner(bufferedReader);
		String payload = "";
		while (sc.hasNextLine()) {
			payload += sc.nextLine() + "\n";
		}
		sc.close();
		return payload;
	}

	public static class MonitorServlet extends HttpServlet {

		private static final long serialVersionUID = 2302036096330714914L;

		@Override
		protected void doPost(HttpServletRequest req, HttpServletResponse resp)
				throws ServletException, IOException {
			
			String payload = getPayload(req.getReader());
			System.out.println("HttpServer - doPost");
			System.out.println("Subscription received - payload:\n" + payload);

			// unmarshalling the notification
			Notification notification = (Notification)mapper.unmarshal(payload);
			NotificationUtil notifUtil = new NotificationUtil();
			
			try {
				if(notification.isVerificationRequest()) {				
					System.out.println("notification is a Request Validation");
					resp.setStatus(HttpServletResponse.SC_OK);
				}
			} catch (NullPointerException e) {
				System.out.println("notification is a Notification Event");
				try {
					Obj object = notifUtil.getObixFromNotification(notification);
				} catch (NotObixContentException f) {
					System.out.println("Error : there is non Obix content ...");
					e.printStackTrace();
				}	
			}

		}
		
	}

	public static void main(String[] args) throws Exception {
		
		// start the server
		Server server = new Server(PORT);
		ServletHandler servletHandler = new ServletHandler();
		
		System.out.println("Creating and joining server ...");
		
		// add servlet and context
		servletHandler.addServletWithMapping(MonitorServlet.class, CONTEXT);
		server.setHandler(servletHandler);
		server.start();
		server.join();
		
	}

}
