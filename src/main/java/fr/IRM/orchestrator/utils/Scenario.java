package fr.IRM.orchestrator.utils;

import java.util.ArrayList;

import fr.IRM.orchestrator.db.Gei;
import fr.IRM.orchestrator.db.Room;

public class Scenario {
	
	public static ArrayList<Integer> tunrOffLightsAfter20h(int time) {
		System.out.println("Scenario : tunrOffLightsAfter21h");
		ArrayList<Integer> doorsToClose = new ArrayList<Integer>();
		Gei gei = Gei.getInstance();
		try {
			for(int i = 0; i < gei.getRooms().size(); i ++) {
				//System.out.println("Room value : " + Integer.toString(gei.getRooms().get(i).getDevices().size()));
				Room tempRoom = gei.getRooms().get(i);
				if(tempRoom.getName().equals("in-gei")) continue;
				if((tempRoom.getDeviceByName("light").getValue() == 1) && 
						(tempRoom.getDeviceByName("presence").getValue() == 0)) {
					System.out.println("tunrOffLightsAfter20h : add light to close !!!");
					doorsToClose.add(i);
				}
			}
		} catch(Exception e) { }
		return doorsToClose;
	}

	public static ArrayList<Integer> closeDoorsAfter20h(int time) {
		System.out.println("Scenario : closeDoorsAfter21h");
		ArrayList<Integer> doorsToClose = new ArrayList<Integer>();
		Gei gei = Gei.getInstance();
		try {
			for(int i = 0; i < gei.getRooms().size(); i ++) {
				//System.out.println("Room value : " + Integer.toString(gei.getRooms().get(i).getDevices().size()));
				Room tempRoom = gei.getRooms().get(i);
				if(tempRoom.getName().equals("in-gei")) continue;
				if((tempRoom.getDeviceByName("door").getValue() == 1) && 
						(tempRoom.getDeviceByName("presence").getValue() == 0)) {
					System.out.println("closeDoorsAfter20h : add door to close !!!");
					doorsToClose.add(i);
				}
			}
		} catch(Exception e) { }
		return doorsToClose;
	}

	public static ArrayList<Integer> closeWindowsAfter20h(int time) {
		System.out.println("Scenario : closeWindowsAfter21h");
		ArrayList<Integer> windowsToClose = new ArrayList<Integer>();
		Gei gei = Gei.getInstance();
		try {
			for(int i = 0; i < gei.getRooms().size(); i ++) {
				Room tempRoom = gei.getRooms().get(i);
				if(tempRoom.getName().equals("in-gei")) continue;
				if((tempRoom.getDeviceByName("window").getValue() == 1) && 
						(tempRoom.getDeviceByName("presence").getValue() == 0)) {
					System.out.println("closeWindowsAfter20h : add window to close !!!");
					windowsToClose.add(i);
				}
			}
		} catch(Exception e) { }
		return windowsToClose;
	}

	public static boolean alarmAfter23h(int time) {
		System.out.println("Scenario : alarmIfPresenceAfter22h");
		boolean alarmToRaise = false;
		Gei gei = Gei.getInstance();
		try {
			boolean alarm = (gei.getRoomByName("in-gei").getDeviceByName("alarm").getValue() == 1) ? true : false;
			for(int i = 0; i < gei.getRooms().size(); i ++) {
				Room tempRoom = gei.getRooms().get(i);
				if(tempRoom.getName().equals("in-gei")) continue;
				if(((tempRoom.getDeviceByName("presence").getValue() == 1) || 
						(tempRoom.getDeviceByName("door").getValue() == 1) || 
						(tempRoom.getDeviceByName("window").getValue() == 1)) && 
						(alarm == false)) {
					System.out.println("alarmAfter23h : set alarm to 1 !!!");
					alarmToRaise = true;
				}
			}
		} catch(Exception e) { }
		return alarmToRaise;
	}
	
	public static ArrayList<Integer> turnOnHeater(int time) {
		System.out.println("Scenario : turnOnHeater");
		ArrayList<Integer> heatersToTurnOn = new ArrayList<Integer>();
		Gei gei = Gei.getInstance();
		try {
			for(int i = 0; i < gei.getRooms().size(); i ++) {
				Room tempRoom = gei.getRooms().get(i);
				if(tempRoom.getName().equals("in-gei")) continue;
				if((tempRoom.getDeviceByName("temp").getValue() <= 15) && 
						(tempRoom.getDeviceByName("heater").getValue() == 0)) {
					System.out.println("turnOnHeater : add heater to turn on !!!");
					heatersToTurnOn.add(i);
				}
			}
		} catch(Exception e) { }
		return heatersToTurnOn;
	} 
	
	public static ArrayList<Integer> turnOffHeater(int time) {
		System.out.println("Scenario : turnOffHeater");
		ArrayList<Integer> heatersToTurnOff = new ArrayList<Integer>();
		Gei gei = Gei.getInstance();
		try {
			for(int i = 0; i < gei.getRooms().size(); i ++) {
				Room tempRoom = gei.getRooms().get(i);
				if(tempRoom.getName().equals("in-gei")) continue;
				if((tempRoom.getDeviceByName("temp").getValue() > 15) && 
						(tempRoom.getDeviceByName("heater").getValue() == 1)) {
					System.out.println("turnOffHeater : add heater to turn off !!!");
					heatersToTurnOff.add(i);
				}
			}
		} catch(Exception e) { }
		return heatersToTurnOff;
	} 
}
