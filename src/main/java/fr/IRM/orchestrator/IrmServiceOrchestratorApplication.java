package fr.IRM.orchestrator;

import javax.annotation.PostConstruct;

import org.eclipse.jetty.server.Server;
import org.eclipse.om2m.commons.server.HttpServer;
import org.eclipse.om2m.commons.server.MonitorServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class IrmServiceOrchestratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(IrmServiceOrchestratorApplication.class, args);
	}
}
