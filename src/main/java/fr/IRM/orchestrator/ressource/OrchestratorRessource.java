package fr.IRM.orchestrator.ressource;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.eclipse.om2m.commons.resource.ContentInstance;
import org.eclipse.om2m.commons.resource.Notification;
import org.eclipse.om2m.commons.server.MonitorServer;
import org.eclipse.om2m.commons.util.NotificationUtil;
import org.eclipse.om2m.commons.util.NotificationUtil.NotObixContentException;
import org.eclispe.om2m.common.mapper.Mapper;
import org.eclispe.om2m.commons.client.Client;
import org.eclispe.om2m.commons.client.Response;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import org.eclipse.jetty.server.Server;

import fr.IRM.orchestrator.db.Device;
import fr.IRM.orchestrator.db.Gei;
import fr.IRM.orchestrator.db.Room;
import fr.IRM.orchestrator.utils.Scenario;
import obix.Obj;
import obix.io.ObixDecoder;

@RestController
@Service
public class OrchestratorRessource {

	private static String baseUrl = "http://localhost:8080/~/";
	private static String originator = "admin:admin";

	private Gei gei = Gei.getInstance();
	private RestTemplate rest = new RestTemplate();
	Mapper mapper = new Mapper();
	private int temp;

	@Scheduled(fixedRate=2500) 
	public void orchestrator() { 

		Gei.getInstance().incrementSimTime();
		int simTime = Gei.getInstance().getSimTime();
		ArrayList<Integer> testArray;
		
		// Turn on heaters if classroom temperature is under 15 degrees
		testArray = Scenario.turnOnHeater(simTime);
		if(testArray.size() > 0) {
			for(int index : testArray) {
				String response;
				response = rest.postForObject("http://localhost:8007/setHeater/" + Gei.getInstance().getRooms().get(index).getName(), null, String.class);
			}
		}
		
		// Turn off heaters if classroom temperature is above 15 degrees
		testArray = Scenario.turnOffHeater(simTime);
		if(testArray.size() > 0) {
			for(int index : testArray) {
				String response;
				response = rest.postForObject("http://localhost:8007/resetHeater/" + Gei.getInstance().getRooms().get(index).getName(), null, String.class);
			}
		}

		// At 20h, close doors, windows and turn off lights if there is nobody in the room
		if(simTime == 20) {
			testArray = Scenario.closeDoorsAfter20h(simTime);
			if(testArray.size() > 0) {
				for(int index : testArray) {
					String response;
					response = rest.postForObject("http://localhost:8004/closeDoor/" + Gei.getInstance().getRooms().get(index).getName(), null, String.class);
				}
			}

			testArray = Scenario.closeWindowsAfter20h(simTime);
			if(testArray.size() > 0) {
				for(int index : testArray) {
					String response;
					response = rest.postForObject("http://localhost:8005/closeWindow/" + Gei.getInstance().getRooms().get(index).getName(), null, String.class);
				}
			}
			
			testArray = Scenario.tunrOffLightsAfter20h(simTime);
			if(testArray.size() > 0) {
				for(int index : testArray) {
					String response;
					response = rest.postForObject("http://localhost:8003/switchLightOff/" + Gei.getInstance().getRooms().get(index).getName(), null, String.class);
				}
			}
		}

		// From 23h to 6h, raise alarm if there is presence in a room or if a window/door is opened
		if(((simTime >= 0) && (simTime < 6)) || ((simTime >= 23) && (simTime <= 24))) {
			if(Scenario.alarmAfter23h(simTime)) {
				String response;
				response = rest.postForObject("http://localhost:8008/setAlarm", null, String.class);
			}
		}
	}

	@RequestMapping(value = "/create/room/{room}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> createRoomOM2M(@PathVariable String room) {
		HttpStatus status = HttpStatus.NOT_FOUND;
		Response r;
		r = rest.getForObject("http://localhost:7001/generate/" + room, Response.class);
		if (r != null) {
			status = HttpStatus.OK;
			//Gei.getInstance().addRoom(r);
		}
		return new ResponseEntity<Response>(r, status);
		//return r;
	}

	@RequestMapping(value = "/create/external", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<Response> createExternalOM2M() {
		Response d;
		HttpStatus status = HttpStatus.NOT_FOUND;
		d = rest.getForObject("http://localhost:7001/generate/external", Response.class);
		if (d != null) {
			status = HttpStatus.OK;
			//gei.setExternalSensors(d);
		}
		return new ResponseEntity<Response>(d, status);
		// return d;
	}

	@RequestMapping(value = "/get/gei", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Gei getGei() {
		return Gei.getInstance();
	}

	@RequestMapping(value = "/{device}/{room}/{state}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> switchDevice(@PathVariable String device, @PathVariable String room, @PathVariable String state) {
		HttpStatus status = HttpStatus.NOT_FOUND;
		String response = "";
		if(state.equals("true")) {
			System.out.println("Switch " + state + " " + device + " in room : " + room);
			switch(device) {
			case "light":
				response = rest.postForObject("http://localhost:8003/switchLightOn/" + room, null, String.class);
				break;
			case "door":
				response = rest.postForObject("http://localhost:8004/openDoor/" + room, null, String.class);
				break;
			case "window":
				response = rest.postForObject("http://localhost:8005/openWindow/" + room, null, String.class);
				break;
			case "presence": 
				response = rest.postForObject("http://localhost:8006/setPresence/" + room, null, String.class);
				break;
			case "heater":
				response = rest.postForObject("http://localhost:8007/setHeater/" + room, null, String.class);
				break;
			}
			if (response != null) {
				status = HttpStatus.OK;
				Gei.getInstance().getRoomByName(room).getDeviceByName(device).setValue(1);
				Gei.getInstance().getRoomByName(room).getDeviceByName(device).addElderValue(1);
			}
		} else if(state.equals("false")) {
			System.out.println("Switch " + state + " " + device + " in room : " + room);
			switch(device) {
			case "light":
				response = rest.postForObject("http://localhost:8003/switchLightOff/" + room, null, String.class);
				break;
			case "door":
				response = rest.postForObject("http://localhost:8004/closeDoor/" + room, null, String.class);
				break;
			case "window":
				response = rest.postForObject("http://localhost:8005/closeWindow/" + room, null, String.class);
				break;
			case "presence": 
				response = rest.postForObject("http://localhost:8006/resetPresence/" + room, null, String.class);
				break;
			case "heater":
				response = rest.postForObject("http://localhost:8007/resetHeater/" + room, null, String.class);
				break;
			}
			if (response != null) {
				status = HttpStatus.OK;
				Gei.getInstance().getRoomByName(room).getDeviceByName(device).setValue(0);
				Gei.getInstance().getRoomByName(room).getDeviceByName(device).addElderValue(0);
			}
		}
		return new ResponseEntity<String>(response, status);
	}

	@RequestMapping(value = "/alarm/{state}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<String> switchAlarm(@PathVariable String state) {
		HttpStatus status = HttpStatus.NOT_FOUND;
		String response = "";
		if(state.equals("true")) {
			System.out.println("Switch " + state + " alarm");
			response = rest.postForObject("http://localhost:8008/setAlarm", null, String.class);
			if (response != null) {
				status = HttpStatus.OK;
				Gei.getInstance().getRoomByName("in-gei").getDeviceByName("alarm").setValue(1);
				Gei.getInstance().getRoomByName("in-gei").getDeviceByName("alarm").addElderValue(1);
			}
		} else if(state.equals("false")) {
			System.out.println("Switch " + state + " alarm");
			response = rest.postForObject("http://localhost:8008/resetAlarm", null, String.class);
			if (response != null) {
				status = HttpStatus.OK;
				Gei.getInstance().getRoomByName("in-gei").getDeviceByName("alarm").setValue(0);
				Gei.getInstance().getRoomByName("in-gei").getDeviceByName("alarm").addElderValue(0);
			}
		}
		return new ResponseEntity<String>(response, status);
	}

	@RequestMapping(value = "/monitor", method = RequestMethod.POST)
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		System.out.println("Notification received");
		String payload = getPayload(req.getReader());
		//System.out.println("HttpServer - doPost");
		//System.out.println("Subscription received - payload:\n" + payload);

		// unmarshalling the notification
		Notification notification = (Notification)mapper.unmarshal(payload);
		NotificationUtil notifUtil = new NotificationUtil();

		try {
			if(notification.isVerificationRequest()) {				
				System.out.println("notification is a Request Validation");
				resp.setStatus(HttpServletResponse.SC_OK);
			}
		} catch (NullPointerException e) {
			System.out.println("notification is a Notification Event");
			try {
				Obj object = notifUtil.getObixFromNotification(notification);
				if(object.has("RoomId") && object.has("Type") && object.has("Value")) {

					// get value and metadatas from the cin
					String roomId = object.get("RoomId").getStr();
					String type = object.get("Type").getStr();
					int value = (int)object.get("Value").getInt();
					System.out.println("Received : " + type + " " + roomId + " " + Integer.toString(value));

					if(gei.isRoomCreated(roomId)) {
						int roomIndex = gei.getRoomIndex(roomId);
						if(gei.getRooms().get(roomIndex).isDeviceCreated(type)) {
							int deviceIndex = gei.getRooms().get(roomIndex).getDeviceIndex(type);
							Gei.getInstance().getRooms().get(roomIndex).getDevices().get(deviceIndex).setValue(value);
							Gei.getInstance().getRooms().get(roomIndex).getDevices().get(deviceIndex).addElderValue(value);
							System.out.println("Room and device created !");
						}
						else {
							Device device = new Device();
							device.setName(type);
							device.setValue(value);
							device.addElderValue(value);
							Gei.getInstance().getRooms().get(roomIndex).addDevice(device);
							System.out.println("Room and not device created !");
						}
					}
					else {
						Device device = new Device();
						device.setName(type);
						device.setValue(value);
						device.addElderValue(value);

						Room room = new Room();
						room.setName(roomId);
						room.setId(roomId);
						room.addDevice(device);
						Gei.getInstance().addRoom(room);
						System.out.println("not Room and not device created !");
					}
				}
			} catch (NotObixContentException f) {
				System.out.println("Error : there is non Obix content ...");
				e.printStackTrace();
			}	
		}

	}

	public static String getPayload(BufferedReader bufferedReader) {
		Scanner sc = new Scanner(bufferedReader);
		String payload = "";
		while (sc.hasNextLine()) {
			payload += sc.nextLine() + "\n";
		}
		sc.close();
		return payload;
	}
}
