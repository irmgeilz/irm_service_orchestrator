package fr.IRM.orchestrator.db;

import java.util.ArrayList;

public class Room {
	
	private String name;
	private String id;
	private ArrayList<Device> devices = new ArrayList<Device>();

	public Room() {
		super();
	}
	
	public Room(String name, String id) {
		super();
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<Device> getDevices() {
		return devices;
	}

	public void setDevices(ArrayList<Device> devices) {
		this.devices = devices;
	}
	
	public void addDevice(Device newDevice) {
		this.devices.add(newDevice);
	}
	
	public boolean isDeviceCreated(String name) {
		boolean result = false;
		for(Device r : this.devices) {
			if(r.getName().equals(name)) result = true;
		}
		return result;
	}
	
	public int getDeviceIndex(String name) {
		int result = 0;
		for(Device d : this.devices) {
			if(d.getName().equals(name)) result = this.devices.indexOf(d);
		}
		return result;
	}
	
	public Device getDeviceByName(String name) {
		Device device = null;
		for(Device d : this.devices) {
			if(d.getName().equals(name)) device = d;
		}
		return device;
	}
}
