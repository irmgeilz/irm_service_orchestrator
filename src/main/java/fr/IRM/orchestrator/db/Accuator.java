package fr.IRM.orchestrator.db;

import java.util.ArrayList;

public class Accuator {

	private String name;
	private String state;
	private ArrayList<String> elderStates;
	
	public Accuator() {
		super();
	}
	
	public Accuator(String name, String state, ArrayList<String> elderStates) {
		super();
		this.name = name;
		this.state = state;
		this.elderStates = elderStates;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public ArrayList<String> getElderStates() {
		return elderStates;
	}

	public void setElderStates(ArrayList<String> elderStates) {
		this.elderStates = elderStates;
	}
	
	public void addElderState(String newState) {
		this.elderStates.add(newState);
	}
}

