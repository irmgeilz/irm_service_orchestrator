package fr.IRM.orchestrator.db;

import java.util.ArrayList;

// Class used to mirror the OM2M architecture and to simulate DataBase
public class Gei {
	
	// Instance of Singleton
	private static Gei INSTANCE = null;
	private ArrayList<Room> rooms = new ArrayList<Room>();
	//private ArrayList<Device> externalSensors = new ArrayList<Device>();
	private int simTime = 0;
	
	// Constructor of Singleton
	private Gei() {}
	
	// Instance getter
	public static synchronized Gei getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new Gei();
		}
		return INSTANCE;
	}

	public int getSimTime() {
		return this.simTime;
	}
	
	public void setSimTime(int time) {
		this.simTime = (time >= 24) ? 0 : time;
	}
	
	public void incrementSimTime() {
		this.simTime = (this.simTime == 24) ? 0 : this.simTime + 1;
		System.out.println("SimTime : " + Integer.toString(simTime));
	}
	
	public ArrayList<Room> getRooms() {
		return this.rooms;
	}

	public void setRooms(ArrayList<Room> rooms) {
		this.rooms = rooms;
	}
	
	public void addRoom(Room newRoom) {
		this.rooms.add(newRoom);
	}
	
	/*public ArrayList<Device> getExternalSensors() {
		return this.externalSensors;
	}

	public void setExternalSensors(ArrayList<Device> externalSensors) {
		this.externalSensors = externalSensors;
	}
	
	public void addExternalSensor(Device newExternalSensor) {
		this.externalSensors.add(newExternalSensor);
	}*/
	
	public boolean isRoomCreated(String name) {
		boolean result = false;
		for(Room r : this.rooms) {
			if(r.getName().equals(name)) result = true;
		}
		return result;
	}
	
	public int getRoomIndex(String name) {
		int result = 0;
		for(Room r : this.rooms) {
			if(r.getName().equals(name)) result = this.rooms.indexOf(r);
		}
		return result;
	}
	
	public Room getRoomByName(String name) {
		Room room = null;
		for(Room r : this.rooms) {
			if(r.getName().equals(name)) room = r;
		}
		return room;
	}
}
